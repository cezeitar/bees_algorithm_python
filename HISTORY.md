# Versions History

## v1.0.2

- minor fixes
- minor code refactoring
- added numpy dependnency
- moved static method *visualize_steps* to instance method *visualize_iteration_steps*

## v1.0.1

- minor fixes in the README

## v1.0.0

- full stable release
- added unit tests
- made compliant with benchmark_functions v1.1.3

## v0.1.1

- initial beta release
